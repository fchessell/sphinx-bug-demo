.. Sphinx Bug Demo documentation master file, created by
   sphinx-quickstart on Mon Oct 29 09:18:46 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Sphinx Bug Demo documentation
=============================

.. toctree::
   :maxdepth: 2

   sphinx_bug_demo
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
