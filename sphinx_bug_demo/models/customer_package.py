from elasticsearch_dsl import InnerDoc, Keyword


class CustomerPackage(InnerDoc):
    """CustomerPackage Model."""

    recipients: Keyword()
    """
    The recipients of the package.
    **Note to Sphinx devs**, removing this attribute description fixes the error.
    """
