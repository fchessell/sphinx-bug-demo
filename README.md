# Sphinx Bug Demo
This repo demonstrates an issue with Sphinx 4.2.0 and ElasticSearchDSL.

## Reproducing the issue
1. Run:<br/>`pip install -r requirements-works.txt`
2. Run and check Sphinx builds correctly:<br/>`sphinx-build -W -v -E -b coverage ./docs/source/docs/build/coverage/`
3. Update Sphinx:<br/> `pip install -r requirements-breaks.txt`
4. Re-run Sphinx build and observe the error:<br/>
`sphinx-build -W -v -E -b coverage ./docs/source/docs/build/coverage/`

## Problematic version
The only difference between these requirements is the Sphinx version.
They were compiled from the corresponding `.in` files, using [piptools](https://pypi.org/project/pip-tools/1.8.0/) (`pip-compile`)

The RST files were built using the command:
`SPHINX_APIDOC_OPTIONS=members sphinx-apidoc -f -o docs/source/ ./`
